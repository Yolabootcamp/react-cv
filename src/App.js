import CV from "./components/CV/CV";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AboutMe from "./components/AboutMe/AboutMe";
import More from "./components/More/More";
import Education from "./components/Education/Education";
import Profile from "./components/Profile/Profile";
import Header from "./components/Header/Header";

const { myInfo, aboutMe, education, languages, softSkills, hardSkills } = CV;

function App() {
  return (
    <>
      <div className="App">
        <div>
          <Router>
            <>
              {/*  <Route exta path="/" component={Home} /> */}
              <Header />
              <Switch>
                <div>
                  <Route path="/profile" component={Profile} />
                  <Route path="/about" component={AboutMe} />
                  <Route path="/education" component={Education} />
                  <Route path="/more" component={More} />
                </div>
              </Switch>
            </>
          </Router>
        </div>

        <div>
          <Profile myInfo={myInfo} />
          <AboutMe aboutMe={aboutMe} />
          <Education education={education} />
          <More
            languages={languages}
            softSkills={softSkills}
            hardSkills={hardSkills}
          />
        </div>
      </div>
    </>
  );
}
export default App;
/**
 <Route exact path="/" component={Home} />
*/
