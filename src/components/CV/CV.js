const CV = {
  myInfo: {
    name: "Yolanda Vicente",
    address: " Sampaio",
    city: "Covilhã, Portugal",
    email: "yolayolandasampaio@gmail.com",
    birthDate: "17/05/1997",
    phone: "(+351) 925356182",
    image: "https://i.ibb.co/XLtg57S/image.png",
    gitlab: "https://gitlab.com/Yolabootcamp",
  },
  aboutMe: [
    {
      info: "I'm Yolanda, I'm 24 years old and I'm a Frontend Developer!",
    },
    {
      info: "I love music, travel, aventure, animals and sometimes read.",
    },
  ],

  education: [
    {
      name: "Bacharel degree in Computer and Electric Engineering",
      date: "2018",
      where: "UBI",
    },
    {
      name: "Master degree in Computer and Electric Engineering",
      date: "2021",
      where: "UBI",
    },
    {
      name: "Frontend Bootcamp from Upgrade Hub and Indra",
      date: "30/09/2021 - 05/11/2021",
      where: "Remote",
    },
    {
      name: "IEFP Course - Digital Marketing - Operationalization",
      date: "30/04/2021",
      where: "Remote",
    },
  ],
  languages: [
    {
      language: "Portuguese",
      wrlevel: "Native",
      spLevel: "Native",
    },
    {
      language: "English",
      wrlevel: "B1",
      spLevel: "B1",
    },
  ],
  softSkills: [
    "Good listener",
    "Ethic",
    "Team worker",
    "Comunication",
    "Commitment",
    "Self-educated",
    "Focus",
  ],
  hardSkills: ["CSS", "HTML", "React JS", "JavaScript", "Bootstrap"],
};

export default CV;
