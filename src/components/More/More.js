import React from "react";
import "./More.css";
import { Container, Row, Col } from "react-bootstrap";

function More({ languages, softSkills, hardSkills }) {
  return (
    <Container>
      <Row>
        <Col sm={12} lg={12}>
          <div className="more-conatiner">
            <h3>More info</h3>
            <div className="more-card">
              <div className="more-languages">
                <h4>Languages</h4>
                {languages.map((item) => {
                  return (
                    <div key={JSON.stringify(item)}>
                      <h5>{item.language}</h5>
                      <p>Writing: {item.wrlevel}</p>
                      <p>Speaking: {item.spLevel}</p>
                    </div>
                  );
                })}
              </div>
            </div>

            <div className="more-skills-container more-card">
              <h4>SoftSkills</h4>
              <div className="more-skills">
                {softSkills.map((item) => {
                  return (
                    <div key={JSON.stringify(item)}>
                      <p>{item}</p>
                    </div>
                  );
                })}
              </div>
            </div>

            <div className="more-skills-container more-card">
              <h4>HardSkills</h4>
              <div className="more-skills">
                {hardSkills.map((item) => {
                  return (
                    <div key={JSON.stringify(item)}>
                      <p>{item}</p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default More;
