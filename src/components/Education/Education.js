import React from "react";
import "./Education.css";
import { TiBook } from "react-icons/ti";
import { Container, Row, Col } from "react-bootstrap";

function Education({ education }) {
  return (
    <Container>
      <Row>
        <Col sm={12} lg={12}>
          <div className="education-container">
            <h3>Education</h3>
            <div className="education-card">
              {education.map((elem) => {
                return (
                  <div key={JSON.stringify(elem)}>
                    <TiBook className="icon" />
                    <p className="education-name">{elem.name}</p>
                    <p> {elem.where} </p>
                    <p> {elem.date}</p>
                  </div>
                );
              })}
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default Education;
