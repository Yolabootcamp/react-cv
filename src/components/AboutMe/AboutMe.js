import React from "react";
import "./AboutMe.css";
import { Container, Row, Col } from "react-bootstrap";

function AboutMe({ aboutMe }) {
  return (
    <Container>
      <Row>
        <Col sm={12} lg={12}>
          <div className="about-container">
            <h3 className="about-name">About Me</h3>
            <div className="about-card">
              {aboutMe.map((item) => {
                return (
                  <div key={JSON.stringify(item)}>
                    <p className="about-info">{item.info}</p>
                  </div>
                );
              })}
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
export default AboutMe;
