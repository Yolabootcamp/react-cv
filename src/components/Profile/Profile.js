import React from "react";
import "./Profile.css";
import { TiCalendar } from "react-icons/ti";
import { TiLocationOutline } from "react-icons/ti";
import { TiPhoneOutline } from "react-icons/ti";
import { FaGitlab } from "react-icons/fa";
import { Container, Row, Col } from "react-bootstrap";

function MyInfo({ myInfo }) {
  return (
    <Container>
      <Row>
        <Col sm={12} lg={12}>
          <section id="profile">
            <div className="main-container">
              <div className="myInfo-container">
                <div className="myCard">
                  <img src={myInfo.image} alt="" className="myInfo-img" />
                  <h1 className="myInfo-name">
                    {myInfo.name}

                    {myInfo.address}
                  </h1>

                  <p>
                    <TiLocationOutline />
                    {myInfo.city}
                  </p>
                  <p>
                    <TiCalendar /> {myInfo.birthDate}
                  </p>
                  <p>
                    <a href={"mailto: " + myInfo.email} className="contact">
                      {" "}
                      {myInfo.email}
                    </a>
                  </p>
                  <TiPhoneOutline />
                  {myInfo.phone}
                  <p>
                    <a href={myInfo.gitlab} className="contact">
                      <FaGitlab /> GitLab
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </section>
        </Col>
      </Row>
    </Container>
  );
}

export default MyInfo;
